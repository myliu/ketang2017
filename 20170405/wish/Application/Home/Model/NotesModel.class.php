<?php
namespace Home\Model;
use Think\Model;
class NotesModel extends Model{
    //定义自动验证

   protected $_validate=   array(

	array('name','require','用户名必须输入！'),
	array('content','require','许愿内容必须输入！'),
    );
      // 定义自动完成

   protected $_auto=   array(

        array('addtime','getTime',1,'callback'),
        );
        protected function getTime()
        {  return date("Y-m-d h:i:s");
        }
}