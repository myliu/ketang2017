<?php
namespace Admin\Controller;
use Think\Controller;
class WishManageController extends CommonController {
    public function index(){ //显示登录成功后的后台管理中心
    
           $notes=M("notes"); 
        $lists=$notes->select(); 
        $this->assign("wish",$lists);
        $this->display();
}

 public function del()  //删除留言
    {
       $id=I('get.id',0);
       $notes=M("notes");
       $rs=$notes->delete($id);   //删除ID那条留言
        if ($rs)
	   {	   	    //设置成功后跳转页面的地址，
          $this->success('删除成功', U('Admin/WishManage/index'));   //在视图.html文件 使用U方法   {:U()}
	   }
	   else
	   {
          //错误页面的默认跳转页面是返回前一页，通常不需要设置

         $this->error('删除失败');

	   }
    }
    }