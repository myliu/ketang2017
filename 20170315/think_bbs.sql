-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2016-01-08 01:28:20
-- 服务器版本： 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `think_bbs`
--
CREATE DATABASE IF NOT EXISTS `think_bbs` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `think_bbs`;

-- --------------------------------------------------------

--
-- 表的结构 `think_data`
--

DROP TABLE IF EXISTS `think_data`;
CREATE TABLE IF NOT EXISTS `think_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `sex` varchar(2) NOT NULL DEFAULT '男' COMMENT '用户性别',
  `message` text COMMENT '留言',
  `imgurl` varchar(255) DEFAULT 'images/1.jpg' COMMENT '用户头像',
  `m_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `think_data`
--

INSERT INTO `think_data` (`id`, `username`, `sex`, `message`, `imgurl`, `m_date`) VALUES
(2, '乔治发', '男', '是ThinkPHP内置的实例化模型的方法', 'images/1.jpg', '2015-03-29 16:15:24'),
(5, '你的名字ff', '女', '这个人很懒,什么都没有写', 'images/4.jpg', '2015-03-29 17:03:49'),
(6, 'adsd', '女', '天天好心情', 'images/3.jpg', '2015-03-29 17:04:25'),
(8, '字', '女', '操作方法', 'images/2.jpg', '2015-03-29 17:07:56'),
(9, '', '男', '这个人很懒,什么都没有写', 'images/1.jpg', '2015-03-29 17:19:37'),
(10, '', '男', '这个人很懒,什么都没有写', 'images/1.jpg', '2015-03-29 17:19:54'),
(11, '', '男', '这个人很懒,什么都没有写', 'images/1.jpg', '2015-03-29 17:25:34'),
(12, '', '男', '这个人很懒,什么都没有写', 'images/2.jpg', '2015-03-29 17:25:48'),
(13, 'mak', '男', 'mak', 'images/3.jpg', NULL),
(14, 'makasdf', '男', 'asdf', 'images/1.jpg', NULL),
(15, 'asdfds', '男', 'asdf', 'images/3.jpg', NULL),
(16, '大爷你的名字', '男', '这个人很懒,什么都没有写', 'images/1.jpg', NULL),
(17, 'asdf', '男', 'asdfasdfasd', 'images/2.jpg', NULL),
(18, 'ergsdfg', '女', '', 'images/3.jpg', NULL),
(19, '', '', '', '', NULL),
(20, '', '', '', '', NULL),
(21, 'cv', '男', '才vsdf才vsdf', 'images/1.jpg', NULL),
(22, '萨答复', '男', '阿斯蒂芬', 'images/1.jpg', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
