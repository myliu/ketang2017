-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2017 年 03 月 02 日 02:09
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `wish`
--
CREATE DATABASE `wish` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `wish`;

-- --------------------------------------------------------

--
-- 表的结构 `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(200) NOT NULL,
  `color` enum('yellow','blue','green') NOT NULL DEFAULT 'yellow',
  `xyz` varchar(100) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `notes`
--

INSERT INTO `notes` (`id`, `content`, `color`, `xyz`, `name`, `addtime`) VALUES
(1, 'jQuery实现拖动层。。。', 'yellow', '584|95|22', 'helloweba', '2011-03-21 10:06:22'),
(2, 'Ajax即时更新保存拖动后的位置。', 'blue', '374|192|24', 'fei', '2011-03-21 10:09:25'),
(3, 'CSS实现层的位置', 'green', '372|40|25', 'angel', '2011-03-21 10:12:17'),
(4, '测试一条啊', 'green', '320|112|25', '月光光', '2011-03-28 12:57:53'),
(5, '测试一是的 而非', 'blue', '339|212|27', '月光光', '2011-03-28 13:03:10');
