-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2017 年 04 月 19 日 03:09
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `think_bbs`
--
CREATE DATABASE `think_bbs` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `think_bbs`;

-- --------------------------------------------------------

--
-- 表的结构 `think_data`
--

CREATE TABLE IF NOT EXISTS `think_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `sex` varchar(2) NOT NULL DEFAULT '男' COMMENT '用户性别',
  `message` text COMMENT '留言',
  `imgurl` varchar(255) DEFAULT 'images/1.jpg' COMMENT '用户头像',
  `m_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- 转存表中的数据 `think_data`
--

INSERT INTO `think_data` (`id`, `username`, `sex`, `message`, `imgurl`, `m_date`) VALUES
(2, '乔治发', '男', '是ThinkPHP内置的实例化模型的方法', 'images/1.jpg', '2015-03-29 16:15:24'),
(5, '你的名字ff', '女', '这个人很懒,什么都没有写', 'images/4.jpg', '2015-03-29 17:03:49'),
(6, 'adsd', '女', '天天好心情', 'images/3.jpg', '2015-03-29 17:04:25'),
(8, '字', '女', '操作方法', 'images/2.jpg', '2015-03-29 17:07:56'),
(9, '', '男', '这个人很懒,什么都没有写', 'images/1.jpg', '2015-03-29 17:19:37'),
(10, '', '男', '这个人很懒,什么都没有写', 'images/1.jpg', '2015-03-29 17:19:54'),
(11, '', '男', '这个人很懒,什么都没有写', 'images/1.jpg', '2015-03-29 17:25:34'),
(12, '', '男', '这个人很懒,什么都没有写', 'images/2.jpg', '2015-03-29 17:25:48'),
(13, 'mak', '男', 'mak', 'images/3.jpg', NULL),
(14, 'makasdf', '男', 'asdf', 'images/1.jpg', NULL),
(15, 'asdfds', '男', 'asdf', 'images/3.jpg', NULL),
(16, '大爷你的名字', '男', '这个人很懒,什么都没有写', 'images/1.jpg', NULL),
(17, 'asdf', '男', 'asdfasdfasd', 'images/2.jpg', NULL),
(18, 'ergsdfg', '女', '', 'images/3.jpg', NULL),
(19, '', '', '', '', NULL),
(20, '', '', '', '', NULL),
(21, 'cv', '男', '才vsdf才vsdf', 'images/1.jpg', NULL),
(22, '萨答复', '男', '阿斯蒂芬', 'images/1.jpg', NULL);
--
-- 数据库: `wish`
--
CREATE DATABASE `wish` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `wish`;

-- --------------------------------------------------------

--
-- 表的结构 `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(200) NOT NULL,
  `color` enum('yellow','blue','green') NOT NULL DEFAULT 'yellow',
  `xyz` varchar(100) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `addtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `notes`
--

INSERT INTO `notes` (`id`, `content`, `color`, `xyz`, `name`, `addtime`) VALUES
(1, 'jQuery实现拖动层。。。', 'yellow', '584|95|22', 'helloweba', '2011-03-21 10:06:22'),
(2, 'Ajax即时更新保存拖动后的位置。', 'blue', '374|192|24', 'fei', '2011-03-21 10:09:25'),
(3, 'CSS实现层的位置', 'green', '372|40|25', 'angel', '2011-03-21 10:12:17'),
(4, '测试一条啊', 'green', '320|112|25', '月光光', '2011-03-28 12:57:53'),
(5, '测试一是的 而非', 'blue', '339|212|27', '月光光', '2011-03-28 13:03:10');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL DEFAULT '',
  `password` char(32) NOT NULL DEFAULT '',
  `logintime` int(10) unsigned NOT NULL,
  `loginip` varchar(30) NOT NULL,
  `lock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `logintime`, `loginip`, `lock`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1377570797, '127.0.0.1', 0),
(7, 'yongjian', '5f9781642c99312ef63aaf7b323666f8', 1377524082, '127.0.0.1', 0),
(6, 'wangwu', '9f001e4166cf26bfbdd3b4f67d9ef617', 1377572162, '127.0.0.1', 0),
(5, 'lisi', 'dc3a8f1670d65bea69b7b65048a0ac40', 1377572140, '127.0.0.1', 0);
