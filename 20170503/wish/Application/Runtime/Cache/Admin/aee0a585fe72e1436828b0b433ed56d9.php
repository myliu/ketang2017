<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus®">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
  <title>Document</title>
  <link rel="stylesheet" href="/ketang2017/20170503/wish/Public/admin/Css/public.css">
      <link rel="stylesheet" href="/ketang2017/20170503/wish/Public/admin/Css/wish.css">
 </head>
 <body>
     
    <div id="box">
        
        <h2 class="h2">愿望列表</h2>
         <form method="get">
         <div id="search">
             请输入关键字查询：
             <input type="text" name="sname" value="<?php echo ($sname); ?>"></input>
             <select name="sop" class="sop">
                 <option <?php if(($sop) == "1"): ?>selected="selected"<?php endif; ?>
                     value="1">按名称查询</option>
                 <option <?php if(($sop) == "2"): ?>selected="selected"<?php endif; ?>
                     value="2">按内容查询</option>
             </select>
             <input type="submit" value="搜索"></input>
         </div>
         </form>
     
	<table class="table">
            
            <tr>
                <th style="width:10%">ID</th>
			<th width="15%">发布者</th>
			<th>内容</th>
			<th width="15%">发布时间</th>
			<th width="10%">操作</th>
		</tr>

		<?php if(is_array($wish)): foreach($wish as $key=>$v): ?><tr>
				<td><?php echo ($v["id"]); ?></td>
				<td><?php echo ($v["name"]); ?></td>
				<td><?php echo ($v["content"]); ?></td>
				<td><?php echo ($v["addtime"]); ?></td>
				<td><a href="<?php echo U('Admin/WishManage/deL',array('id' => $v['id'])),'';?>">删除</a></td>
			</tr><?php endforeach; endif; ?>
		<tr>
			<td colspan='5' align='center'>
				<?php echo ($page); ?>
			</td>
		</tr>
	</table>
       </div>

 </body>
</html>