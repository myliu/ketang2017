<?php
namespace Admin\Controller;
use Think\Controller;
class LoginController extends Controller {
   
     public function login(){//显示登录的页面
       $this->display();
    }
    
    public function code(){   //生成验证码
        $verify=new \Think\Verify();
        $verify->entry();
    }
    
     public function dologin(){//点击登录按钮
       if (IS_POST)  //检验是否有数据提交
       {
           //第一步：首先判断验证码是否正确
           $rst=$this->checkcode(I('post.code'));  //I('post.code') 获取用户输入的验证码
           if ($rst===false)
               $this->error('验证码错误');
           //第二步：检查用户名和密码
           $username=I('post.username');
           $password=I('post.password');
           //2.1 查询数据中，有没有这个用户
           $User=M("user");
           $userdata=$User->where("username='$username'")->find();//如果数据库里有这个用户，则用户的数据会保存在$userdata,否则$userdata的值就会为null
           if ($userdata!=null)
           {
                $md5password=md5($password);
                if ($md5password==$userdata['password'])
                {
                   //成功登录的标志
                    session('loginname',$username);
                    $this->success('登录成功，请稍等',U('Admin/Index/index')); 
                }
                 else 
                 {
                   $this->error('登录失败，密码错误！'); 
                 }
           }
           else
           {
               $this->error('登录失败，用户不存在！');  
           }
       }
    }
    
    private function checkcode($code,$codeid='')
    {
        //$code 用户输入的验证码  $codeid是系统产生的验证码
       $verify=new \Think\Verify();
       return $verify->check($code,$codeid);
    }
    
    public function logout(){   //退出
        //退出，第一要求清除登录成功的标志  第二跳转登录页
        session('[destroy]');
        $this->success("退出成功",U("Admin/Login/login"));
    }
    
    
}