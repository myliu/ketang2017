<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){  
//      $r=array();
//	$r['uname']='蓝天';
//	$r['meg']='天气真好';
//	$this->assign('row',$r);
        //从数据库读数据
        $bbsdata=M("think_data");
        $data=$bbsdata->order('id desc')->select();//取think_data 的数据到$data    二维数组  $data[1]['username']
        $bbscount=$bbsdata->count();//查询留言的记录数
        $this->assign("bbscount",$bbscount);
        $this->assign("rows",$data);
        $this->display();//显示BBS首页
    }
}