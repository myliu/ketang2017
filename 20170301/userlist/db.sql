-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2017 年 03 月 02 日 02:08
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `db`
--
CREATE DATABASE `db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db`;

-- --------------------------------------------------------

--
-- 表的结构 `tb_users`
--

CREATE TABLE IF NOT EXISTS `tb_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(255) NOT NULL COMMENT '用户名',
  `set` tinyint(4) NOT NULL DEFAULT '1' COMMENT '性别',
  `mm` varchar(255) NOT NULL COMMENT '密码',
  `hm` float NOT NULL COMMENT '电话号码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `tb_users`
--

INSERT INTO `tb_users` (`id`, `name`, `set`, `mm`, `hm`) VALUES
(1, '张三', 1, '123456', 13132100000),
(2, '白送', 1, '123456', 12345700),
(3, '李四', 0, '654321', 13132100000),
(7, '上班', 1, '123456', 1234570000),
(10, '随便', 1, '123456', 1234570000);
