<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){  
//      $r=array();
//	$r['uname']='蓝天';
//	$r['meg']='天气真好';
//	$this->assign('row',$r);
        //从数据库读数据
        $bbsdata=M("think_data");   //模型--数据库里的表
        $data=$bbsdata->order('id desc')->select();//取think_data 的数据到$data    二维数组  $data[1]['username']
        $bbscount=$bbsdata->count();//查询留言的记录数
        $this->assign("bbscount",$bbscount);
        $this->assign("rows",$data);
        $this->display();//显示BBS首页
    }
    
    public function del()  //删除留言
    {
       $id=I('get.id',0);
       $bbsdata=M("think_data");
       $rs=$bbsdata->delete($id);   //删除ID那条留言
        if ($rs)
	   {	   	    //设置成功后跳转页面的地址，
          $this->success('删除成功', U('Home/Index/index'));   //在视图.html文件 使用U方法   {:U()}
	   }
	   else
	   {
          //错误页面的默认跳转页面是返回前一页，通常不需要设置
         $this->error('删除失败');
	   }
    }
    public function  edit(){
	 	//调用视图显示
        $id=I('get.id',0);
	//实例化模型类
       $model=M('think_data');
//	//获取该id的留言
        $data=$model->where("id=$id")->find();
        $this->assign("row",$data);
	$this->display();
       
	 }
          public function  update(){
              //保存修改后留言
              //先收集用户修改过的留言信息
              if(IS_POST){
                  $id=I('get.id',0);//获取地址栏的ID
                  $data['username']=I('pst.username','');
                  $data['sex']=I('pst.sex','男');
                  $data['message']=I('pst.message','');
                  $data['imgurl']=I('pst.imgurl','');
                  $data['m_date']=('date.y-m-d h:i:s');
                  $model=M('think_data');
                  $model->where("id=$id")->save($data);
                  $this->success('修改完成！',U('Home/Index/index'));
              }
              else{
                  $this->error('非法操作！');
              }
          }

    
       
}