<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
public function index(){
        $notes=M("notes"); 
        $lists=$notes->select(); 
        $this->assign("rows",$lists);
        $this->display();
    }
        public function del()  //删除留言
    {
       $id=I('get.id',0);
       $notes=M("notes");
       $rs=$notes->delete($id);   //删除ID那条留言
        if ($rs)
	   {	   	    //设置成功后跳转页面的地址，
          $this->success('删除成功', U('Home/Index/index'));   //在视图.html文件 使用U方法   {:U()}
	   }
	   else
	   {
          //错误页面的默认跳转页面是返回前一页，通常不需要设置

         $this->error('删除失败');

	   }
    }
        public function add()  //无异步刷新发表留言
    {
       //完成第一个添加到数据库的任务，如果完成，跳转回到index         

       $model=D('notes');      

       if ($model->create())  //   获取表单POST过来的数据创建数据对象 有数据提交
       {    
            $model->addtime=date('Y-m-d H:i:s',time());  //$model->m_date  是数据表中日期的字段名 常见错误写成：m_data  data数据  date日期
            $result = $model->add(); // 写入数据到数据库 
            if($result){
               // 如果主键是自动增长型 成功后返回值就是最新插入的值

                //设置成功后跳转页面的地址，

                   $this->success('许愿成功', U('Home/Index/index')); 

            }
            else
            {
                 //错误页面的默认跳转页面是返回前一页，通常不需要设置

                  $this->error('许愿失败');

            }  
       }
       else  //数据通不过验证
       {
           $this->error($model->getError());  // 将出错的验证信息显示
       }
       
    }
    public function ajaxadd(){ 
       $model = D("notes"); // 实例化model对象  `
       if($data=$model->create()){
           $d=date('Y-m-d H:i:s',time()); //为创建的数据对象新增一个日期值 
           $model->addtime=$d;
            $result = $model->add(); // 写入数据到数据库 
            if($result){
              $data['id']=$result;
              $data['addtime']=$d;
              $data['isok']=1;
              $this->ajaxReturn($data); 
             }
              else
              {
                   //错误页面的默认跳转页面是返回前一页，通常不需要设置

                  $data['isok']=0;

                  $data['msg']='许愿失败！';
               $this->ajaxReturn($data);

              }
        }
        else {
            $data['isok']=0;
            $data['msg']=$model->getError();
            $this->ajaxReturn($data);  // 将出错的验证信息显示 
        }
       
    }
       public function ajaxdel(){       
       $id=I('get.id',0);
	//实例化模型类



	$model=M('notes'); 	     

	$rs=$model->delete($id);
	if ($rs)
	   { //设置成功后跳转页面的地址，
            $data['isok']=1;
          $this->ajaxReturn($data);  
	   }
	   else
	   { //错误页面的默认跳转页面是返回前一页，通常不需要设置
           $data['isok']=0;
          $this->ajaxReturn($data);  
	   }
    }
}