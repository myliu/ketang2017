<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang=zh-cn>
<head>
	<meta charset=utf-8 />
	<title>新闻中心</title>
	<link rel="stylesheet" href="/phpke/20170315/taian/Public/css/base.css" />
	<link rel="stylesheet" href="/phpke/20170315/taian/Public/css/news.css" />
	<link href="/phpke/20170315/taian/Public/images/Limited.ico" rel="shortcut icon" />
</head>
<body>
	<div class="header" id="header">
		<div class="header_top clear">
			<div class="top_warp fr clear">
				<a href="contact.html">联系我们</a>
				<a href="contact.html">设为首页</a>
				<a href="contact.html">收藏本站</a>
				<form action="#" id="search" class="clear">
					<input type="text" class="text" value="请输入关键字搜索" /><font><input class="submit" type="submit" value="| 搜索" /></font>
				</form>
				
			</div>
		</div>
		<div class="header_nav clear">
			<img src="/phpke/20170315/taian/Public/images/logo.gif" />
			<div class="nav_wrap">
				<span><font>服务热线：</font><strong>888-888888</strong></span>
				<ul class="clear">
					<li ><a href="/phpke/20170315/taian/index.php/Home/index/index">首页</a></li>
					<li class="index"><a href="/phpke/20170315/taian/index.php/Home/news/index">新闻中心</a></li>
					<li><a href="product.html">产品展示</a></li>
					<li><a href="company.html">公司介绍</a></li>
					<li><a href="contact.html">联系我们</a></li>
					<li><a href="enterprise.html">企业介绍</a></li>
					<li><a href="strength.html">公司实力</a></li>
					<li><a href="file.html">诚信档案</a></li>
				</ul>
			</div>
			
		</div>
		<div class="banner" id="banner">
			<a class="pre"></a><a class="next"></a>
			<div class="banner_wrap">
				<ul class="action clear">
					<li><a href="#"><img src="/phpke/20170315/taian/Public/images/banner/banner1.jpg"></a></li>
					<li><a href="#"><img src="/phpke/20170315/taian/Public/images/banner/banner1.jpg"></a></li>
					<li><a href="#"><img src="/phpke/20170315/taian/Public/images/banner/banner1.jpg"></a></li>
				</ul>
				<div class="clear"><p class="index"></p><p></p><p></p></div>	
			</div>
		</div>
	</div>
	<div id="content" class="content">
		<div class="section_a Ns clear">
			<div class="title">
				<span class="icon"><h2>企业新闻</h2><p>News</p></span>
			</div>
			<ul class="text">
				<li><a href="/phpke/20170315/taian/index.php/Home/news/detail">轨道交通发展前景</a><font>2015-01-05</font></li>
				<li><a href="/phpke/20170315/taian/index.php/Home/news/detail">轨道交通发展前景</a><font>2015-01-05</font></li>
				<li><a href="/phpke/20170315/taian/index.php/Home/news/detail">轨道交通发展前景</a><font>2015-01-05</font></li>
				<li><a href="/phpke/20170315/taian/index.php/Home/news/detail">轨道交通发展前景</a><font>2015-01-05</font></li>
				<li><a href="/phpke/20170315/taian/index.php/Home/news/detail">轨道交通发展前景</a><font>2015-01-05</font></li>
			</ul>
			<span class="list"><a href="index.html">首页</a><a href="#">上一页</a><a href="#">下一页</a><a href="#">尾页</a><font>页次：1/1页 </font><font>共<strong>5</strong>条记录</font></span>
		</div>
		<div style="height: 85px;">
			<div id="return"><span>返回顶部</span></div>
		</div>
	</div>
	
	<div id="footer" class="footer">
	<div class="partner clear">
		<font>友情链接：</font>
		<a href="#" class="shouhu"><img src="/phpke/20170315/taian/Public/images/shouhu.jpg" alt="sohu" title="搜狐"/></a>
		<a href="#"><img src="/phpke/20170315/taian/Public/images/tengxun.jpg" alt="tenxun" title="腾讯"/></a>
		<a href="#"><img src="/phpke/20170315/taian/Public/images/xinlang.jpg" alt="xinlang" title="新浪"/></a>
		<a href="#"><img src="/phpke/20170315/taian/Public/images/wangyi.jpg" alt="wangyi" title="网易"/></a>
		<a href="#" class="last"><img src="/phpke/20170315/taian/Public/images/baidu.jpg" alt="baidu" title="百度"/></a>
	</div>
		<p class="message"><span>地址：广州市天河路365号天俊阁1202</span><span>电话：020 38812035</span><span>传真：020 38812035</span><span>Email：88888888@xx.com</span></p>
		<p class="copyright">Copyright ©2010-2015 泰安交通安全设施有限公司 </p>
		<p class="icp">版权所有 | ICP备案序号:蜀ICP备12003782号-1  宜宾市公安局网监支队备案号5115000202009</p>
	</div>
	</body>
<script type="text/javascript" src="js/index.js"></script>
<!--[if IE 6]>
	<script src="js/DD_belatedPNG_0.0.8a.js"></script>
	<script>
	DD_belatedPNG.fix('*');
	</script>
<![endif]-->
</html>