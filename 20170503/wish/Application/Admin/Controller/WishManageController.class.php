<?php
namespace Admin\Controller;
use Think\Controller;
class WishManageController extends CommonController {
    public function index(){ //显示登录成功后的后台管理中心
         $sname=I('get.sname','');
         $sop=I('get.sop',1);
         $wish=M("notes");
         $p=I('get.p',1);
         
         if ($sop==1){
              $wishda=$wish->where("name like '%$sname%' ")->page($p.',5')->select();//分页
         //查询记录数
         $count=$wish->where("name like '%$sname%' ")->count();
         }
        else{
            $wishda=$wish->where("content like '%$sname%' ")->page($p.',5')->select();//分页
         //查询记录数
         $count=$wish->where("content like '%$sname%' ")->count();
        }
         //生成分页的链接

         $PAGE=new \Think\Page($count,5);//实例化分页类 传入总记录数和每页显示的记录数(5)

         $PAGE->setConfig('header','共有%TOTAL_PAGE%页,当前是第%NOW_PAGE%页<br/>');

         $PAGE->setConfig('first','首页');
         $PAGE->setConfig('last','尾页');
         
         $PAGE->setConfig('prev','上一页');
         $PAGE->setConfig('next','下一页');
         $PAGE->setConfig('theme','%HEADER% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
         
                 
         //生成分页链接

         
        $page->lastSuffix = false;

         $show=$PAGE->show();
         $this->assign('sname',$sname);
         $this->assign('sop',$sop);
         $this->assign("wish",$wishda);
         $this->assign("page",$show);//视图里使用page显示分页链接
        $this->display();
}

 public function del()  //删除留言
    {
       $id=I('get.id',0);
       $notes=M("notes");
       $rs=$notes->delete($id);   //删除ID那条留言
        if ($rs)
	   {	   	    //设置成功后跳转页面的地址，
          $this->success('删除成功', U('Admin/WishManage/index'));   //在视图.html文件 使用U方法   {:U()}
	   }
	   else
	   {
          //错误页面的默认跳转页面是返回前一页，通常不需要设置

         $this->error('删除失败');

	   }
    }
    
    
    
    
}