<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang=zh-cn>
<head>
	<meta charset=utf-8 />
	<title>首页</title>
	<link rel="stylesheet" href="/phpke/20170315/taian/Public/css/base.css" />
	<link rel="stylesheet" href="/phpke/20170315/taian/Public/css/index.css" />
	<link href="/phpke/20170315/taian/Public/images/Limited.ico" rel="shortcut icon" />
</head>
<body>
	<div class="header" id="header">
		<div class="header_top clear">
			<div class="top_warp fr clear">
				<a href="contact.html">联系我们</a>
				<a href="contact.html">设为首页</a>
				<a href="contact.html">收藏本站</a>
				<form id="search" action="#" class="clear">
					<input type="text" class="text" value="请输入关键字搜索" /><font><input class="submit" type="submit" value="| 搜索" /></font>
				</form>
				
			</div>
		</div>
		<div class="header_nav clear">
			<img src="/phpke/20170315/taian/Public/images/logo.gif" />
			<div class="nav_wrap">
				<span><font>服务热线：</font><strong>888-888888</strong></span>
				<ul class="clear">
					<li class="index"><a href="/phpke/20170315/taian/index.php/Home/index/index">首页</a></li>
					<li><a href="/phpke/20170315/taian/index.php/Home/news/index">新闻中心</a></li>
					<li><a href="product.html">产品展示</a></li>
					<li><a href="company.html">公司介绍</a></li>
					<li><a href="contact.html">联系我们</a></li>
					<li><a href="enterprise.html">企业介绍</a></li>
					<li><a href="strength.html">公司实力</a></li>
					<li><a href="file.html">诚信档案</a></li>
				</ul>
			</div>
			
		</div>
		<div class="banner" id="banner">
			<a class="pre"></a><a class="next"></a>
			<div class="banner_wrap">
				<ul class="action clear">
					<li><a href="#"><img src="/phpke/20170315/taian/Public/images/banner/banner1.jpg"></a></li>
					<li><a href="#"><img src="/phpke/20170315/taian/Public/images/banner/banner1.jpg"></a></li>
					<li><a href="#"><img src="/phpke/20170315/taian/Public/images/banner/banner1.jpg"></a></li>
				</ul>
				<div class="clear"><p class="index"></p><p></p><p></p></div>	
			</div>
		</div>
	</div>
	<div id="content" class="content">
		<div class="wrap clear">
			<div class="section_a Ns clear">
				<div class=" title clear">
					<span class="icon"><h2>企业新闻</h2><p>News</p></span><a class="more" href="news.html"></a>
				</div>
				<ul class="text">
					<li class="clear"><span class="time"><h3>05</h3><p>01.2015</p></span><span><h3><a href="#">中央今年首轮巡视瞄准26家央企严查靠山吃</a></h3><p>去年第三轮中央巡视刚结束向各巡视点反馈情况，今年首轮中央巡视即将启动。两轮巡视有一个共同点：锁定央企……</p></span></li>
					<li class="clear"><span class="time"><h3>05</h3><p>01.2015</p></span><span><h3><a href="#">中央今年首轮巡视瞄准26家央企严查靠山吃</a></h3><p>去年第三轮中央巡视刚结束向各巡视点反馈情况，今年首轮中央巡视即将启动。两轮巡视有一个共同点：锁定央企……</p></span></li>
					<li class="clear"><span class="time"><h3>05</h3><p>01.2015</p></span><span><h3><a href="#">中央今年首轮巡视瞄准26家央企严查靠山吃</a></h3><p>去年第三轮中央巡视刚结束向各巡视点反馈情况，今年首轮中央巡视即将启动。两轮巡视有一个共同点：锁定央企……</p></span></li>
					<li class="clear"><span class="time"><h3>05</h3><p>01.2015</p></span><span><h3><a href="#">中央今年首轮巡视瞄准26家央企严查靠山吃</a></h3><p>去年第三轮中央巡视刚结束向各巡视点反馈情况，今年首轮中央巡视即将启动。两轮巡视有一个共同点：锁定央企……</p></span></li>
				</ul>
			</div>
			<div class="section_a Pr">
				<div class="title">
					<span class="icon"><h2>产品分类</h2><p>Product</p></span>
				</div>
				<ul class="product">
					<li><a href="#">安全背心</a></li>
					<li><a href="#">安全告示牌</a></li>
					<li><a href="#">反光马甲</a></li>
					<li><a href="#">网布背心</a></li>
					<li><a href="#">交通指示牌</a></li>
					<li><a href="#">三角警示牌</a></li>
				</ul>
			</div>
		</div>
		<div class="main clear">
			<img src="/phpke/20170315/taian/Public/images/about.jpg" alt="about_us" title="关于我们" />
			<div class="section_a Ab">
				<div class="title">
					<span class="icon"><h2>关于我们</h2><p>About us</p></span>
				</div>
				<p class="text1">深圳泰安交通安全设施有限公司位于万里长江第一城—宜宾。公司是专业从事道路交通安全设施（标志、标牌、标线、波形护栏、交通信号灯、交通电子警察、路灯照明、停车场设施、城市亮化、边坡治理、公路路面、土石方、园林绿化工程实施作业）的规划、设计、生产、施工和安装的综合性大型企业。公司严格按照国家行业标准GB57682009执行生产和安装。</p>
				<p class="text1">我公司拥有一批专业技术人员以及经验丰富的施工队伍，公司建立健全完善管理体系，并已在行业中取得一定的成绩，公司所完成的工程项目均以质量优良，信誉良好著称，受到业主及管理部门的普遍好评。</p>
			</div>
		</div>
		<div class="section_a Cu clear">
			<div class="title">
					<span class="icon"><h2>企业理念</h2><p>Cultural</p></span>
			</div>
			<div class="text">
				<img src="/phpke/20170315/taian/Public/images/Cultural.jpg" alt="Cultural" title="企业理念" />
					<p>企业理念是企业在持续经营和长期发展过程中，继承企业优良传统，适应时代要求，由企业家积极倡导，全体员工自觉实践，从而形成的代表企业信念、激发企业活力、推动企业生产经营的团体精神和行为规范。按照不同层次划分为精神文化型和组织制度型两类。企业理念是企业在持续经营和长期发展过程中，继承企业优良传统，适应时代要求，由企业家积极倡导，全体员工自觉实践，从而形成的代表企业信念、激发企业活力、推动企业生产经营的团体精神和行为规范。按照不同层次划分为精神文化型和组织制度型两类。</p>
			</div>
		</div>
		<div class="section_a Ho">
			<div class="title">
					<span class="icon"><h2>企业荣誉</h2><p>Honor of qualification</p></span>
			</div>
			<div class="photo"><img src="/phpke/20170315/taian/Public/images/honor.jpg" alt="honor1" title="" /><img src="/phpke/20170315/taian/Public/images/honor.jpg" alt="honor1" title="" /><img src="/phpke/20170315/taian/Public/images/honor.jpg" alt="honor1" title="" /><img src="/phpke/20170315/taian/Public/images/honor.jpg" alt="honor1" title="" /><img src="/phpke/20170315/taian/Public/images/honor.jpg" alt="honor1" title="" /></div>
		</div>
		<div class="section_a Sh">
			<div class="title">
					<span class="icon"><h2>产品展示</h2><p>Product</p></span>
			</div>
			<ul class="clear">
				<li><font><img src="/phpke/20170315/taian/Public/images/product.jpg" alt="product1" title="产品1" /></font><a href="product_details">方形雪糕筒、雪糕筒规格介绍、东莞雪糕筒、雪糕筒广告牌</a></li>
				<li><font><img src="/phpke/20170315/taian/Public/images/product.jpg" alt="product1" title="产品1" /></font><a href="product_details">方形雪糕筒、雪糕筒规格介绍、东莞雪糕筒、雪糕筒广告牌</a></li>
				<li><font><img src="/phpke/20170315/taian/Public/images/product.jpg" alt="product1" title="产品1" /></font><a href="product_details">方形雪糕筒、雪糕筒规格介绍、东莞雪糕筒、雪糕筒广告牌</a></li>
				<li><font><img src="/phpke/20170315/taian/Public/images/product.jpg" alt="product1" title="产品1" /></font><a href="product_details">方形雪糕筒、雪糕筒规格介绍、东莞雪糕筒、雪糕筒广告牌</a></li>
			</ul>
		</div>
		<div style="height: 85px;">
		<div id="return"><span>返回顶部</span></div>
		</div>
	</div>
	
	<div id="footer" class="footer">
	<div class="partner clear">
		<font>友情链接：</font>
		<a href="#" class="shouhu"><img src="/phpke/20170315/taian/Public/images/shouhu.jpg" alt="sohu" title="搜狐"/></a>
		<a href="#"><img src="/phpke/20170315/taian/Public/images/tengxun.jpg" alt="tenxun" title="腾讯"/></a>
		<a href="#"><img src="/phpke/20170315/taian/Public/images/xinlang.jpg" alt="xinlang" title="新浪"/></a>
		<a href="#"><img src="/phpke/20170315/taian/Public/images/wangyi.jpg" alt="wangyi" title="网易"/></a>
		<a href="#" class="last"><img src="/phpke/20170315/taian/Public/images/baidu.jpg" alt="baidu" title="百度"/></a>
	</div>
		<p class="message"><span>地址：广州市天河路365号天俊阁1202</span><span>电话：020 38812035</span><span>传真：020 38812035</span><span>Email：88888888@xx.com</span></p>
		<p class="copyright">Copyright ©2010-2015 泰安交通安全设施有限公司 </p>
		<p class="icp">版权所有 | ICP备案序号:蜀ICP备12003782号-1  宜宾市公安局网监支队备案号5115000202009</p>
	</div>
	</body>
<script type="text/javascript" src="js/index.js"></script>
<!--[if IE 6]>
	<script src="js/DD_belatedPNG_0.0.8a.js"></script>
	<script>
	DD_belatedPNG.fix('*');
	</script>
<![endif]-->
</html>